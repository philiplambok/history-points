class User < ApplicationRecord
  has_secure_password
  validates :username, presence: true, uniqueness: true

  belongs_to :role
  has_many :histories
  has_one :point

  def add_point 
    if point.total >= 100 
      point.total = 0
    else 
      point.total += 10
    end
    point.save
  end

  def admin? 
    role_id == 1 ? true : false
  end

  def member?
    role_id == 0 ? true : false 
  end
end
