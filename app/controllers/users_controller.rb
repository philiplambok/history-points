class UsersController < ApplicationController
  before_action :must_auth, only: [:new, :create]
  before_action :must_admin, only: [:new, :create]
  
  def new 
    @user = User.new
  end

  def create
    @user = User.new user_params

    if @user.save 
      Point.create(user: @user)
      render "confirmation"
    else 
      render 'new'
    end
  end

  private 
  def user_params
    params.require(:user).permit(:username, :password, :password_confirmation)
  end

  def must_auth
    redirect_to login_path unless current_user
  end

  def must_admin
    redirect_to login_path unless current_user.admin?
  end
end
