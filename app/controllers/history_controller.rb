class HistoryController < ApplicationController
  before_action :set_user, only: [:create]
  before_action :set_history, only: [:create]

  def create
    @history.content = "#{DateTime.now.strftime("%d %B %Y")} anda melakukan cukur."
    if @history.save 
      @user.add_point
      redirect_to dashboard_path
    end
  end

  private 
  def set_user 
    @user = User.find_by(id: params[:users])
  end

  def set_history 
    @history = @user.histories.build 
  end
end
