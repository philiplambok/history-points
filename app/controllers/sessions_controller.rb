class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(username: params[:username])
    if user && user.authenticate(params[:password])
      sign_in(user)
      redirect_to dashboard_path
    else 
      redirect_to login_path, notice: "Credential is invalid"
    end
  end

  def destroy
    logout
    redirect_to login_path 
  end
end
