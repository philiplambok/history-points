class DashboardController < ApplicationController
  before_action :current_user?, only: [ :index ]

  def index
  end

  private
  def current_user?
    redirect_to login_path unless signed_in?
  end
end
